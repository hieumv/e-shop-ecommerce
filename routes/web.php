<?php



use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;

use App\Http\Controllers\Frontend\BlogsController;
use App\Http\Controllers\Frontend\MemberRegisterController;
use App\Http\Controllers\Frontend\MemberLoginController;
use App\Http\Controllers\Frontend\CommentController;
use App\Http\Controllers\Frontend\AccountController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\ShopController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// route frontend
Route::get('/index',function(){
    return view('frontend.layouts.app');
});

// member
Route::get('/member-register', [MemberRegisterController::class,'register'])->name('member-register');
Route::post('/member-register', [MemberRegisterController::class,'postRegister'])->name('postRegister');
Route::get('/member-login', [MemberLoginController::class,'login'])->name('member-login');
Route::post('/member-login', [MemberLoginController::class,'postLogin'])->name('postLogin');
Route::get('/member-logout', [MemberLoginController::class,'logout'])->name('member-logout');

// blog
Route::get('/blogs',[BlogsController::class,'index'])->name('blogs');
Route::get('/blogDetail/{id}',[BlogsController::class,'blogDetail'])->name('blogDetail');
Route::post('/blogs/rate/ajax',[BlogsController::class,'rate_ajax']);

// comment
Route::post('/blogs/comment/{id}',[CommentController::class,'postComment'])->name('postComment');
Route::post('/blogs/rep-comment/{id}',[CommentController::class,'repComment'])->name('repComment');

// account
Route::middleware('member')->group(function(){
    Route::get('/account/update',[AccountController::class,'index'])->name('account');
    Route::post('/account/update',[AccountController::class,'postUpdateUser'])->name('postUpdateUser');
    Route::get('/account/my-product',[ProductController::class,'index'])->name('listProduct');
    Route::get('/account/add-product',[ProductController::class,'addProduct'])->name('addProduct');
    Route::post('/account/add-product',[ProductController::class,'postAdd'])->name('postAdd-product');
    Route::get('/account/edit-product/{id}',[ProductController::class,'editProduct'])->name('editProduct');
    Route::post('/account/edit-product/{id}',[ProductController::class,'postEdit'])->name('postEdit-product');
    Route::get('/account/delete-product/{id}',[ProductController::class,'deleteProduct'])->name('deleteProduct');
});


// shop
Route::get('/',[ShopController::class,'index'])->name('shop');
Route::get('/detailProduct/{id}',[ShopController::class,'detail'])->name('detailProduct');
Route::post('/cart/id/ajax',[ShopController::class,'addToCart']);
// search
Route::post('/',[ShopController::class,'searchProduct'])->name('search');
Route::get('/search-advanced',[ShopController::class,'searchAdvanced'])->name('search-advanced');
Route::post('/search-advanced',[ShopController::class,'postSearch']);
Route::post('/search/price/ajax',[ShopController::class,'searchPrice']);

// cart
Route::get('/cart',[CartController::class,'cart'])->name('cart');
Route::post('/cart/id/delete-cart',[CartController::class,'cartDelete']);
Route::post('/cart/id/up-cart',[CartController::class,'cartUp']);
Route::post('/cart/id/down-cart',[CartController::class,'cartDown']);

// checkout
Route::get('/check-out',[CheckoutController::class,'index'])->name('checkOut');
Route::post('/check-out',[CheckoutController::class,'checkOut']);
Route::get('/login-check-out',[CheckoutController::class,'loginCheckOut'])->name('loginCheckOut');
Route::post('/login-check-out',[CheckoutController::class,'postLoginCheckout']);


Auth::routes();

Route::middleware('admin.member')->group(function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/dashboard', function () {
        return view('admin.dashboard.dashboard');
    })->name('dashboard');

    Route::get('/profile', [UserController::class,'index'])->name('profile');
    Route::post('/profile', [UserController::class,'UpdateUser'])->name('profile-post');
    // country
    Route::get('/country', [CountryController::class,'index'])->name('list-countries');
    Route::get('/add-country', [CountryController::class,'add'])->name('add-country');
    Route::post('/add-country', [CountryController::class,'postAdd'])->name('postAdd-country');
    Route::get('/delete-country/{id}', [CountryController::class,'delete'])->name('delete-country');

    // category
    Route::get('/category', [CategoryController::class,'index'])->name('list-categories');
    Route::get('/add-category', [CategoryController::class,'add'])->name('add-category');
    Route::post('/add-category', [CategoryController::class,'postAdd'])->name('postAdd-category');
    Route::get('/delete-category/{id}', [CategoryController::class,'delete'])->name('delete-category');

    // brand
    Route::get('/brand', [BrandController::class,'index'])->name('list-brands');
    Route::get('/add-brand', [BrandController::class,'add'])->name('add-brand');
    Route::post('/add-brand', [BrandController::class,'postAdd'])->name('postAdd-brand');
    Route::get('/delete-brand/{id}', [BrandController::class,'delete'])->name('delete-brand');

    // Blog
    Route::get('/blog',[BlogController::class,'index'])->name('blog');
    Route::get('/creat-blog',[BlogController::class,'creat'])->name('creat-blog');
    Route::post('/creat-blog',[BlogController::class,'postCreat'])->name('postCreat-blog');
    Route::get('/delete-blog/{id}',[BlogController::class,'delete'])->name('delete-blog');
    Route::get('/edit-blog/{id}',[BlogController::class,'edit'])->name('edit-blog');
    Route::post('/edit-blog/{id}',[BlogController::class,'postEdit'])->name('postEdit-blog');
    // Route::get('/blogDetail/{id}',[BlogController::class,'showBlog'])->name('blogDetail');
});










