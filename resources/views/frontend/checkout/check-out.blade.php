@extends('frontend.layouts.app')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Check out</li>
            </ol>
        </div><!--/breadcrums-->
        
            <div class="register-req">
                <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
            </div><!--/register-req-->
            <form action="" method="post">
                <div class="shopper-informations">
                    <div class="row">
                        {{-- <div class="col-sm-3">
                            <div class="shopper-info">
                                <p>Shopper Information</p>
                                <form>
                                    <input type="text" placeholder="Display Name">
                                    <input type="text" placeholder="User Name">
                                    <input type="password" placeholder="Password">
                                    <input type="password" placeholder="Confirm password">
                                </form>
                                <a class="btn btn-primary" href="">Get Quotes</a>
                                <a class="btn btn-primary" href="">Continue</a>
                            </div>
                        </div> --}}
                        <div class="col-sm-5 clearfix">
                            <div class="bill-to">
                                <p>Bill To</p>
                                <div class="form-one">
                                        <input type="text" placeholder="Email" name="email">
                                        <input type="text" placeholder="Name" name="name">
                                        <input type="text" placeholder="Address" name="address">
                                        <input type="text" placeholder="Phone" name="phone">
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="order-message">
                                <p>Shipping Order</p>
                                <textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
                                <label><input type="checkbox"> Shipping to bill address</label>
                            </div>	
                        </div>					
                    </div>
                </div>
                <div class="review-payment">
                    <h2>Review & Payment</h2>
                </div>
        
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item</td>
                                <td class="description"></td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sessionCart as $item)
                            <tr>
                                <td class="cart_product">
                                    <a href=""><img src="upload/product/hinh_85_{{$item['hinhanh'][0]}}" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{$item['name']}}</a></h4>
                                    <p>Web ID: 1089772</p>
                                </td>
                                <td class="cart_price">
                                    <p>${{intval($item['price'])}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up"  id="{{$item['id']}}"> + </a>
                                        <input class="cart_quantity_input" type="text" name="quantity" value="{{$item['qty']}}" autocomplete="off" size="2">
                                        <a class="cart_quantity_down" id="{{$item['id']}}"> - </a>
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$item['qty']*intval($item['price'])}}</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete"  id="{{$item['id']}}"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            
                        @endforeach
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td colspan="2">
                                    <table class="table table-condensed total-result">
                                        <tbody>
                                            <tr>
                                                <td>Cart Sub Total</td>
                                                <td>${{$totalOrder}}</td>
                                            </tr>
                                            <tr>
                                                <td>Exo Tax</td>
                                                <td>$0</td>
                                            </tr>
                                            <tr class="shipping-cost">
                                                <td>Shipping Cost</td>
                                                <td>Free</td>										
                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td class="totalOrder"><span>${{$totalOrder}}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="submit" name="order" class="btn btn-primary" value="ORDER">
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @csrf
            </form>
            
       
        <div class="payment-options">
                <span>
                    <label><input type="checkbox"> Direct Bank Transfer</label>
                </span>
                <span>
                    <label><input type="checkbox"> Check Payment</label>
                </span>
                <span>
                    <label><input type="checkbox"> Paypal</label>
                </span>
            </div>
    </div>
</section> <!--/#cart_items-->
<script>
    
</script>
@endsection