<div>
    <h1>hi{{$name}}</h1>
    <h1>email: {{$dataHistory['email']}}</h1>
    <h1>name: {{$dataHistory['name']}}</h1>
    <h1>phone: {{$dataHistory['phone']}}</h1>
    <table border="1">
        <tr>
            <td>name</td>
            <td>qty</td>
            <td>price</td>
        </tr>
        @foreach ($sessionCart as $item)
            <tr>
                <td>{{$item['name']}}</td>
                <td>{{$item['qty']}}</td>
                <td>{{$item['price']}}</td>
            </tr>
        @endforeach
        <tfoot>
            <span>total price: {{$totalOrder}}</span>
        </tfoot>
    </table>
    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Non reiciendis mollitia accusamus.</p>
</div>