@extends('frontend/layouts/app')

@section('content')
@include('frontend/layouts/slider')
    <div class="container">
        <div class="row">
            @include('frontend/layouts/left-sidebar')
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Result Search!</h2>
                    <div class="row">
                        <form action="" method="post">
                            @csrf
                            <div class="col-sm-3 search_box pull-right">
                                <input type="text" name="name" placeholder="name">
                            </div>
                            <div class="col-sm-3">
                                <select name="price" id="">
                                    <option value="">Price</option>
                                    <option value="2">10000 - 20000$</option>
                                    <option value="3">Tren 20000$</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="category_id" id="">
                                    <option value="">Choose Category</option>
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="brand_id" id="">
                                    <option value="">Choose Brand</option>
                                    @foreach ($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            
                            <button class="btn btn-primary" type="submit" style="margin-left: 12px;">Search</button>
                        </form>
                    </div>
                    <div id="price-range">
                        @if (isset($products))
                        @foreach ($products as $item)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="upload/product/{{'hinh_329_'.$item->hinhanh[0]}}" alt="" />
                                            <h2>${{intval($item->price)}}</h2>
                                            <p>{{$item->name}}</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content">
                                                <a href="{{route('detailProduct',['id'=>$item->id])}}">
                                                    <h2>${{intval($item->price)}}</h2>
                                                    <p>{{$item->name}}</p>
                                                </a>
                                                <a class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                <input type="hidden" class="id-product" value="{{$item->id}}">
                                            </div>
                                        </div>
                                </div>
                                
                            </div>
                        </div>
                        {{-- {{$products->links()}} --}}
                        @endforeach
                        
                        @endif
                    </div>
                    
                </div><!--features_items-->
                
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('a.add-to-cart').click(function(){
                var idProduct = $(this).next().val();
                alert(idProduct);
                var qtyCart = parseInt($('li.qtyCart').find('span').text());
                qtyCart+=1;
                $('li.qtyCart').find('span').text(qtyCart);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type:'POST',
                    url:"{{url('/cart/id/ajax')}}",
                    data:{
                        id: idProduct,
                    },
                    success:function(data){
                        console.log(data.success);
                    }
                });
            })

            $('.slider-horizontal').click(function(){
                var xx= $(".tooltip-inner").text();
                var min = xx.split(' ')[0];
                var max = xx.split(' ')[2];
                console.log(min);
                console.log(max);

                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });

                $.ajax({
                    type:'POST',
                    url:"{{url('/search/price/ajax')}}",
                    data:{
                        min: min,
                        max: max
                    },
                    dataType: 'json',

                    success:function(response){
                        var productListHtml = '';
                        $.each(response.products, function(index, product) {
                            productListHtml += '<div class="col-sm-4">';
                            productListHtml += '<div class="product-image-wrapper">';
                            productListHtml += '<div class="single-products">';
                            productListHtml += '<div class="productinfo text-center">';
                            productListHtml += '<img src="upload/product/hinh_329_'+product.hinhanh[0]+'" alt="" />';
                            productListHtml += '<h2>$'+parseInt(product.price)+'</h2>';
                            productListHtml += '<p>'+product.name+'</p>';
                            productListHtml += '<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>';
                            productListHtml += '</div>';
                            productListHtml += '<div class="product-overlay">';
                            productListHtml += '<div class="overlay-content">';
                            productListHtml += '<a href="'+'{{route("detailProduct",["id"=>'+product.id+'])}}'+'">';
                            productListHtml += '<h2>$'+parseInt(product.price)+'</h2>';
                            productListHtml += '<p>'+product.name+'</p>';
                            productListHtml += '</a>';
                            productListHtml += '<a class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>';
                            productListHtml += '<input type="hidden" class="id-product" value="'+product.id+'">';
                            productListHtml += '</div>';
                            productListHtml += '</div>';
                            productListHtml += '</div>';
                            productListHtml += '</div>';
                            productListHtml += '</div>';
                        });
                        $('#price-range').html(productListHtml);
                        
                    },
                });

            })
            
        });
    </script>


@endsection