@extends('frontend/layouts/app')

@section('content')
@include('frontend/layouts/slider')
    <div class="container">
        <div class="row">
            @include('frontend/layouts/left-sidebar')
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Result Search!</h2>
                    @foreach ($products as $item)
                        <div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="upload/product/{{'hinh_329_'.$item->hinhanh[0]}}" alt="" />
											<h2>${{intval($item->price)}}</h2>
											<p>{{$item->name}}</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<a href="{{route('detailProduct',['id'=>$item->id])}}">
                                                    <h2>${{intval($item->price)}}</h2>
												    <p>{{$item->name}}</p>
                                                </a>
												<a class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                <input type="hidden" class="id-product" value="{{$item->id}}">
											</div>
										</div>
								</div>
								
							</div>
						</div>
                    @endforeach
                </div><!--features_items-->
                
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('a.add-to-cart').click(function(){
                var idProduct = $(this).next().val();
                alert(idProduct);
                var qtyCart = parseInt($('li.qtyCart').find('span').text());
                qtyCart+=1;
                $('li.qtyCart').find('span').text(qtyCart);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type:'POST',
                    url:"{{url('/cart/id/ajax')}}",
                    data:{
                        id: idProduct,
                    },
                    success:function(data){
                        console.log(data.success);
                    }
                });
            })
        });
    </script>


@endsection