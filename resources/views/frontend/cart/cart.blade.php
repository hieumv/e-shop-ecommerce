@extends('frontend.layouts.app')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sessionCart as $item)
                        <tr>
                            <td class="cart_product">
                                <a href=""><img src="upload/product/hinh_85_{{$item['hinhanh'][0]}}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$item['name']}}</a></h4>
                                <p>Web ID: 1089772</p>
                            </td>
                            <td class="cart_price">
                                <p>${{intval($item['price'])}}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up"  id="{{$item['id']}}"> + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$item['qty']}}" autocomplete="off" size="2">
                                    <a class="cart_quantity_down" id="{{$item['id']}}"> - </a>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">${{$item['qty']*intval($item['price'])}}</p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete"  id="{{$item['id']}}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        
                    @endforeach
                   

                    
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            
                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                        
                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                    @if (isset($totalOrder))
                        <li class="total_order">Cart Sub Total <span>${{$totalOrder}}</span></li>
                        <li>Eco Tax <span>$0</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li class="total">Total <span>${{$totalOrder}}</span></li>
                        {{-- total_order --}}
                    @else
                        <li class="total_order">Cart Sub Total <span>$0</span></li>
                        <li>Eco Tax <span>$0</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li class="total">Total <span>$</span></li>
                    @endif
                    </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="{{route('checkOut')}}">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->
<script>
    $(document).ready(function(){ 
        // xoa
        $("a.cart_quantity_delete").click(function(){
            var getId = $(this).attr("id");
            $(this).closest("tr").remove();

            var qtyCart = parseInt($("li.qtyCart").find("span").text());
            var qty = parseInt($(this).closest("tr").find("input.cart_quantity_input").val());
            qtyCart -= qty;
            $("li.qtyCart").find("span").text(qtyCart);

            // order total
            var totalPrice_str = $(this).closest("tr").find("p.cart_total_price").text();
            totalPrice = parseInt(totalPrice_str.replace("$",""));

            
            var totalOrder_str = $("li.total_order").find("span").text();
            totalOrder = parseInt(totalOrder_str.replace("$",""));
            totalOrder -= totalPrice;
            $("li.total_order").find("span").text("$"+totalOrder);
            $("li.total").find("span").text("$"+totalOrder);

            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type:'POST',
                url:"{{url('/cart/id/delete-cart')}}",
                data:{
                    id: getId,
                },
                success:function(data){
                    console.log(data.success);
                }
            });
            
            
        })

        // tang +
        var qtyUp = 0;
        $("a.cart_quantity_up").click(function(){
            var getId = $(this).attr("id");
            // alert(getId);
            var qty = parseInt($(this).next("input").val());
            qtyUp = qty + 1;
            var qtyCart = parseInt($("li.qtyCart").find("span").text());
            
            qtyCart += 1;
            $("li.qtyCart").find("span").text(qtyCart);


            $(this).next("input").attr("value",qtyUp);
            // tinh tong
            var price_str = $(this).closest("tr").find("td.cart_price p").text();
            price = parseInt(price_str.replace("$",""));
            priceTotal = price*qtyUp;

            var totalOrder_str = $("li.total_order").find("span").text();
            totalOrder = parseInt(totalOrder_str.replace("$",""));
            totalOrder += price;
            $("li.total_order").find("span").text("$"+totalOrder);
            $(this).closest("tr").find("p.cart_total_price").text("$"+priceTotal);
            $("li.total").find("span").text("$"+totalOrder);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type:'POST',
                url:"{{url('/cart/id/up-cart')}}",
                data:{
                    id: getId,
                },
                success:function(data){
                    console.log(data.success);
                }
            });
        })

        // giam -
        var qtyDown = 0;
        $("a.cart_quantity_down").click(function(){
            var getId = $(this).attr("id");
            var qty = parseInt($(this).closest("td.cart_quantity").find("input").val());
            qtyDown = qty - 1;

            var price_str = $(this).closest("tr").find("td.cart_price p").text();
            price = parseInt(price_str.replace("$",""));
            priceTotal = price*qtyDown;
            // alert(priceTotal)
            $(this).closest("tr").find("p.cart_total_price").text("$"+priceTotal)
            if(qtyDown == 0){
                $(this).closest("tr").remove();
            }

            if(qtyDown > 0){
                $(this).closest("td.cart_quantity").find("input").attr("value",qtyDown);
            }
            
            var qtyCart = parseInt($("li.qtyCart").find("span").text());
            qtyCart -= 1;
            if(qtyCart == 0){
                $("li.qtyCart").find("span").text("");
            }else{
                $("li.qtyCart").find("span").text(qtyCart);
            }

            var totalOrder_str = $("li.total_order").find("span").text();
            totalOrder = parseInt(totalOrder_str.replace("$",""));
            totalOrder -= price;
            $("li.total_order").find("span").text("$"+totalOrder);
            $("li.total").find("span").text("$"+totalOrder);
            $(this).closest("tr").find("p.cart_total_price").text("$"+priceTotal);

            
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type:'POST',
                url:"{{url('/cart/id/down-cart')}}",
                data:{
                    id: getId,
                },
                success:function(data){
                    console.log(data.success);
                }
            });

    })
})         
</script>
@endsection