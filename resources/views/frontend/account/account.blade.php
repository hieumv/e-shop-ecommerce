
@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            @include('Frontend/layouts/account-sidebar')
            <div class="right col-sm-6 col-sm-offset-2">
                <h3>Update account</h3>
                <div class="login-form"><!--login form-->
                    <h3></h3>
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="text" placeholder="" value="{{$user->name}}" name="name"/>
                        <input type="email" placeholder="" value="{{$user->email}}" name="email"/>
                        <input type="password" placeholder="password" value="" name="password"/>
                        <input type="text" placeholder="Dia chi" value="{{$user->address}}" name="address"/>
                        <div class="form-group">
                            <select name="id_country" id="">
                                @foreach ($countries as $country)
                                    <option value="{{$country->id}}" @if ($country->id == $user->id_country)
                                        selected
                                    @endif>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="file" name="avatar">
                        </div>
                        <div class="form-group">
                            <p>Old Avatar</p>
                            <img src="../upload/user/avatar/{{$user->avatar}}" alt="" width="200px">
                        </div>
                        <!-- <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span> -->
                        @csrf
                        <button type="submit" class="btn btn-default" name="update-btn">Update</button>
                    </form>
                </div><!--/login form-->
        
            </div>
        </div>        
    </div>
    
</section><!--/form-->
@endsection