@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible">
                        <h4>{{session('success')}}</h4>    
                    </div>                            
                    @endif
                    @if (session('error'))
                    <div class="alert alert-success alert-dismissible">
                        <h4>{{session('error')}}</h4>    
                    </div>                            
                    @endif
                    <form action="" method="post">
                        <input type="text" placeholder="Email" name="email"/>
                        @error('email')
                            <span style="color:red;">{{$message}}</span>
                        @enderror
                        <input type="password" placeholder="Password" name="password"/>
                        @error('password')
                            <span style="color:red;">{{$message}}</span>
                        @enderror
                        <span>
                            <input type="checkbox" class="checkbox" name="remember_me"> 
                            Keep me signed in
                        </span>
                        <button type="submit" class="btn btn-default">Login</button>
                        @csrf
                    </form>
                    <button class="btn">check</button>
                </div><!--/login form-->
            </div>
        </div>
    </div>
    
</section><!--/form-->
@endsection