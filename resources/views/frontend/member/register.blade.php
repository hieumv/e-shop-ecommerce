@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible">
                        <h4>{{session('success')}}</h4>    
                    </div>                            
                @endif
                {{-- @if ($errors->any())
                    <div class="alert alert-warning alert-dismissible">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach    
                        </ul>    
                    </div>  
                @endif --}}
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    <form action="" method="post">
                        <input type="text" placeholder="Name" name="name"/>
                        @error('name')
                            <span style="color:red">{{$message}}</span>
                        @enderror
                        <input type="text" placeholder="Email Address" name="email"/>
                        @error('email')
                            <span style="color:red">{{$message}}</span>
                        @enderror
                        <input type="password" placeholder="Password" name="password"/>
                        @error('password')
                            <span style="color:red">{{$message}}</span>
                        @enderror
                        <button type="submit" class="btn btn-default">Signup</button>
                        @csrf
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->
@endsection