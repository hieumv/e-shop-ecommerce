
@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            
            @include('Frontend/layouts/account-sidebar')
            <div class="right col-sm-6 col-sm-offset-2">
                <h3>Add Product</h3>
                <div class="login-form">
                    <h3></h3>
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="text" placeholder="Name" value="" name="name"/>
                        <span>
                            @error('name')
                                {{$message}}
                            @enderror
                        </span>
                        <input type="text" placeholder="Price" value="" name="price"/>
                        <span>
                            @error('price')
                                {{$message}}
                            @enderror
                        </span>
                        <select class="form-select" name="id_category">
                            <option value="">Choose Category...</option>
                            @foreach ($categories as $category)                            
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <span>
                            @error('id_category')
                                {{$message}}
                            @enderror
                        </span>
                        <br><br>
                        <select class="form-select" name="id_brand">
                            <option value="">Choose Brands...</option>
                            @foreach ($brands as $brand)                            
                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                            @endforeach
                        </select>
                        <span>
                            @error('id_brand')
                                {{$message}}
                            @enderror
                        </span>
                        <br><br>
                        <select name="status" class="form-select">
                            <option value="0">New</option>
                            <option value="1">Sale</option>
                        </select>
                        <br><br>
                        <input type="text" name="sale" placeholder="0%">
                        <input type="text" placeholder="Company" value="" name="company"/>
                        <span>
                            @error('company')
                                {{$message}}
                            @enderror
                        </span>
                        
                        <div class="form-group">
                            <input type="file" name="hinhanh[]" multiple>
                        </div>
                        <span>
                            @error('image')
                                {{$message}}
                            @enderror
                        </span>
                        <textarea name="detail" id="" cols="30" rows="5">

                        </textarea>
                        
                
                        <!-- <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span> -->
                        @csrf
                        <button type="submit" class="btn btn-default">Add Product</button>
                        
                    </form>
                </div><!--/login form-->
                
            </div>
        </div>        
    </div>
    
</section><!--/form-->
@endsection