
@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            @include('Frontend/layouts/account-sidebar')
            <div class="right col-sm-8 col-sm-offset-1">
                <h3>List Product</h3>
                <section id="cart_items">			
                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <th style="width: 10%">ID</th>
                                    <th style="width: 20%">Name</th>
                                    <th style="width: 20%">Image</th>
                                    <th style="width: 15%">Price</th>
                                    <th style="width: 15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($list_products)
                                    @foreach ($list_products as $product)
                                        <tr>
                                            <td>{{$product->id}}</td>                            
                                            <td>{{$product->name}}</td>                            
                                            <td><img src="../upload/product/{{'hinh_85_'.$product->hinhanh[0]}}" alt=""></td>                            
                                            <td>${{intval($product->price)}}</td>                                            
                                            <td class="">
                                                <a class="cart_quantity_delete" href="{{route('deleteProduct',['id'=>$product->id])}}"><i class="fa fa-times"></i></a>
                                                <a class="cart_quantity_update" href="{{route('editProduct',['id'=>$product->id])}}"><i class="fa fa-pencil"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach 
                                @endif     
                            </tbody>
                        </table> 
                    </div>
                    <a href="{{route('addProduct')}}" class="btn btn-primary float-right">Add Product</a>
                
                </section>
                 <!--/#cart_items-->
                
            </div>
        </div>        
    </div>
    
</section><!--/form-->
@endsection