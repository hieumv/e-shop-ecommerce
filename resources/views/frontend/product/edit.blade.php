
@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            @include('Frontend/layouts/account-sidebar')
            <div class="right col-sm-6 col-sm-offset-2">
                <h3>{{$title}}</h3>
                <div class="login-form">
                    <h3></h3>
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="text" placeholder="Name" value="{{$product->name}}" name="name"/>
                        <span>
                            @error('name')
                                {{$message}}
                            @enderror
                        </span>
                        <input type="text" placeholder="Price" value="{{$product->price}}" name="price"/>
                        <span>
                            @error('price')
                                {{$message}}
                            @enderror
                        </span>
                        <select class="form-select" name="id_category">
                            @foreach ($categories as $category)                            
                                <option value="{{$category->id}}" @if ($product->id_category == $category->id)
                                    @selected(true)
                                @endif>{{$category->name}}</option>
                            @endforeach
                        </select>
                        <span>
                            @error('id_category')
                                {{$message}}
                            @enderror
                        </span>
                        <br><br>
                        <select class="form-select" name="id_brand">
                            @foreach ($brands as $brand)                            
                            <option value="{{$brand->id}}" @if ($product->id_brand == $brand->id)
                                @selected(true)
                            @endif>{{$brand->name}}</option>
                            @endforeach
                        </select>
                        <span>
                            @error('id_brand')
                                {{$message}}
                            @enderror
                        </span>
                        <br><br>
                        <select name="status" class="form-select">
                            <option value="0">New</option>
                            <option value="1">Sale</option>
                        </select>
                        <br><br>
                        <input type="text" name="sale" placeholder="0%" value="{{$product->sale}}">
                        <input type="text" placeholder="Company" value="{{$product->company}}" name="company"/>
                        <span>
                            @error('company')
                                {{$message}}
                            @enderror
                        </span>
                        <div class="row">
                            @foreach ($product['hinhanh'] as $image)
                                <div class="col-sm-2 col-sm-offset-1">
                                    <img src="../../upload/product/hinh_85_{{$image}}" alt="">
                                    <input type="checkbox" style="height: 20px" name="image_delete[]" value="{{$image}}">
                                </div>
                            @endforeach
                            
                        </div>
                        <div class="form-group">
                            <input type="file" name="hinhanh[]" multiple>
                        </div>
                        <span>
                            @error('hinhanh')
                                {{$message}}
                            @enderror
                        </span>
                        @if (session('error-upload'))
                            <span>{{session('error-upload')}}</span>                         
                        @endif
                        <textarea name="detail" id="" cols="30" rows="5">
                            {{$product->detail}}
                        </textarea>
                
                        <!-- <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span> -->
                        @csrf
                        <button type="submit" class="btn btn-default">Edit Product</button>
                        
                        
                    </form>
                </div><!--/login form-->
        
            </div>
            
        </div>        
    </div>
    
</section><!--/form-->
@endsection