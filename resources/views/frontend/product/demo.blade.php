
@extends('frontend/layouts/app')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            @include('Frontend/layouts/account-sidebar')
            <div class="right col-sm-8 col-sm-offset-1">
                <h3>List Product</h3>
                <section id="cart_items">			
                    
                    @foreach ($product['hinhanh'] as $image)
                        <img src="../upload/product/{{$image}}" alt="">
                    @endforeach

                
                </section>
                 <!--/#cart_items-->
                
            </div>
        </div>        
    </div>
    
</section><!--/form-->
@endsection