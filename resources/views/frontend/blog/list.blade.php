@extends('frontend/layouts/app')
@section('content')
<section>
    <div class="container">
        <div class="row">
            @include('frontend.layouts.left-sidebar')
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Latest From our Blog</h2>
                    @foreach ($dataBlogs as $Blog)
                        <div class="single-blog-post">
                        <a href="{{route('blogDetail',['id'=>$Blog->id])}}"><h3>{{$Blog->title}}</h3></a>
                        
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> Mac Doe</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                            </span>
                        </div>
                        <a href="">
                            <img src="upload/Blog/Image/{{$Blog->image}}" alt="">
                        </a>
                        <p>{{$Blog->description}}</p>
                        <a  class="btn btn-primary" href="">Read More</a>
                    </div>
                    @endforeach
                    
                    
                    <div class="pagination-area">
                        {{-- <ul class="pagination">
                            <li><a href="" class="active">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
                        </ul> --}}
                        {{ $dataBlogs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</section><!--/form-->
@endsection