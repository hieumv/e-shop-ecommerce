@extends('frontend/layouts/app')

@section('content')
<section>
    <div class="container">
        <div class="row">
            
            @include('frontend.layouts.left-sidebar')
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Latest From our Blog {{$blog->id}}</h2>
                    <div class="single-blog-post">
                        <h3>{{$blog->title}}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> Mac Doe</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <!-- <span>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </span> -->
                        </div>
                        <a href="">
                            {{-- <img src="../upload/Blog/Image/{{$blog->image}}" alt=""> --}}
                        </a>
                        {!! $blog->content !!}
                        <div class="pager-area">
                            <ul class="pager pull-right">
                                @if(!$previous)
                                    <li><a href="{{route('blogDetail',['id'=>$next]) }}">Next</a></li>
                                @elseif(!$next)
                                    <li ><a href="{{route('blogDetail',['id'=>$previous]) }}">Previous</a></li>
                                @else
                                    <li><a href="{{route('blogDetail',['id'=>$next]) }}">Next</a></li>
                                    <li ><a href="{{route('blogDetail',['id'=>$previous]) }}">Previous</a></li>
                                @endif
                               
                                

                               
                            </ul>
                        </div>
                    </div>
                </div><!--/blog-post-area-->

                <div class="rating-area">
                    
                    <script type="text/javascript">
    	
                        $(document).ready(function(){
                            //vote
                            $('.ratings_stars').hover(
                                // Handles the mouseover
                                function() {
                                    $(this).prevAll().andSelf().addClass('ratings_hover');
                                    // $(this).nextAll().removeClass('ratings_vote'); 
                                },
                                function() {
                                    $(this).prevAll().andSelf().removeClass('ratings_hover');
                                    // set_votes($(this).parent());
                                }
                            );
                
                            $('.ratings_stars').click(function(){
                                var check ="{{Auth::check()}}";
                                
                                if(check==true){
                                    var Values =  $(this).find("input").val();
                                    alert(Values);
                                    $(this).closest(".vote").find(".rate-np").text(Values);
                                    if ($(this).hasClass('ratings_over')) {
                                        $('.ratings_stars').removeClass('ratings_over');
                                        $(this).prevAll().andSelf().addClass('ratings_over');
                                    } else {
                                        $(this).prevAll().andSelf().addClass('ratings_over');
                                    }

                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    $.ajax({
                                        type:'POST',
                                        url:"{{url('/blogs/rate/ajax')}}",
                                        data:{
                                            rate:Values,
                                            id_user:"{{Auth::id()}}",
                                            id_blog:{{$blog->id}}
                                        },
                                        success:function(data){
                                            console.log(data.success);
                                        }
                                    });
                                }else{
                                    
                                    alert("Login Please");
                                
                                }
                            });

                            // xu ly comment
                            $('.postCmt').click(function(){
                                var check = "{{Auth::check()}}";
                                if(check==true){
                                    return true;
                                }else{
                                    alert("Login Please");
                                    return false;
                                }
                            })

                            $('.repBtn').click(function() {
                                // kiem tra login
                                var check = "{{Auth::check()}}";
                                if(check==true){
                                    // dung js lay id_user cua cmt cha
                                    var level = $(this).closest('.media-body').find('.level').val();
                                    // hien ra form de co text area de reply cmt
                                    var html = '<form action="{{route('repComment',['id'=>$blog->id])}}" method="post">'+
                                                '<div class="text-area">'+
                                                '<textarea name="comment" rows="6"></textarea>'+
                                                '<input type="hidden" value="'+level+'" name="level">'+
                                                '<button type="submit" class="btn btn-primary" id="">Reply</button>'+
                                                '</div>'+
                                                '@csrf'+
                                                '</form>;';
                                    $(this).parent().append(html);
                                    return true;
                                }else{
                                    alert("Login Please");
                                    return false;
                                }
                                
                            });

                        });
                    </script>
                    
                    <div class="rate">
                        <div class="vote">
                            
                            {{-- <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
                            <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
                            <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
                            <div class="star_5 ratings_stars"><input value="5" type="hidden"></div> --}}
                            @for ($i = 1; $i < 6; $i++)
                                <div class="star_{{$i}} ratings_stars {{$i <= $avgRate ? 'ratings_over' : ''}}"><input value="{{$i}}" type="hidden"></div>
                            @endfor
                            <span class="rate-np">{{$avgRate}}</span>
                        </div> 
                    </div>
                    
                    
                   
                </div><!--/rating-area-->

                <div class="socials-share">
                    <a href=""><img src="images/blog/socials.png" alt=""></a>
                </div><!--/socials-share-->

                <!-- <div class="media commnets">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="images/blog/man-one.jpg" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Annie Davis</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <div class="blog-socials">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <a class="btn btn-primary" href="">Other Posts</a>
                        </div>
                    </div>
                </div> --><!--Comments-->
                <div class="response-area">
                    <h2>3 RESPONSES</h2>
                    <ul class="media-list">
                            @foreach ($dataComment as $comment)
                                @if ($comment->level == 0)
                                    <li class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="images/blog/man-two.jpg" alt="">
                                        </a>
                                        <div class="media-body">
                                            <ul class="sinlge-post-meta">
                                                <li><i class="fa fa-user"></i>{{$comment->name}}</li>
                                                {{-- <li><i class="fa fa-clock-o"></i> 1:33 pm</li> --}}
                                                <li><i class="fa fa-calendar"></i> {{$comment->created_at}}</li>
                                                <input type="hidden" value="{{$comment->id}}" class="level">
                                            </ul>
                                            <p>{{$comment->cmt}}</p>
                                            <a class="btn btn-primary repBtn" ><i class="fa fa-reply"></i>Replay</a>
                                        </div>
                                    </li>
                                    @foreach ($dataRepComment as $repComment)
                                        @if ($repComment->level == $comment->id)
                                            <li class="media second-media">
                                                <a class="pull-left" href="#">
                                                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                                                </a>
                                                <div class="media-body">
                                                    <ul class="sinlge-post-meta">
                                                        <li><i class="fa fa-user"></i>{{$repComment->name}}</li>
                                                        {{-- <li><i class="fa fa-clock-o"></i> 1:33 pm</li> --}}
                                                        <li><i class="fa fa-calendar"></i> {{$repComment->created_at}}</li>
                                                    </ul>
                                                    <p>{{$repComment->cmt}}</p>
                                                    <a class="btn btn-primary repBtn"><i class="fa fa-reply"></i>Replay</a>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach 
                                @endif
                            @endforeach
                            
                        

                        {{-- <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-three.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                        
                        <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-three.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li> --}}
                        {{-- <li class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-four.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                        <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-three.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                        <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-three.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                        <li class="media second-media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-three.jpg" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li> --}}
                    </ul>					
                </div><!--/Response-area-->
                <div class="replay-box">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{route('postComment',['id'=>$blog->id])}}" method="post">
                                <h2>Leave a replay</h2>
                                <div class="text-area">
                                <div class="blank-arrow">
                                    <label>Your Name</label>
                                </div>
                                <span>*</span>
                                <textarea name="comment" rows="11"></textarea>
                                <button type="submit" class="btn btn-primary postCmt" >Post Comment</button>
                
                                </div>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div><!--/Repaly Box-->
                {{-- <script>
                    $(document).ready(function(){
                        
                    })
                </script> --}}
            </div>
        </div>
    </div>
    
</section>
</section><!--/form-->

@endsection