@extends('admin.layouts.app')
@section('content')
    
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">{{$title}}</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach    
                            </ul>    
                        </div>  
                        @endif
                        <form action="" method="post">
                                <div class="form-group">
                                    <label><h4>Name</h4></label>
                                    <input type="text" class="form-control" value="" name="name">
                                    @error('name')
                                            <span style="color:red">{{$message}}</span>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success">
                                    Add Category
                                </button>
                            @csrf
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
      
@endsection