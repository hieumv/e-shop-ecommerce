@extends('admin.layouts.app')
@section('content')
    
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">{{$title}}</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"> <div class="col-12">
                                @if (session('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <h4>{{session('success')}}</h4>    
                                    </div>                            
                                    @endif

                                @if ($errors->any())
                                <div class="alert alert-warning alert-dismissible">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach    
                                </ul>    
                        </div>  
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Blog</h4>
                                
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Desription</th>
                                            <th scope="col">Content</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($dataBlogs as $blog)
                                                <tr>
                                                    <th scope="row">{{$blog->id}}</th>
                                                    <td>{{$blog->title}}</td>
                                                    <td>{{$blog->image}}</td>
                                                    <td>{{$blog->description}}</td>
                                                    <td>{{$blog->content}}</td>
                                                    <td><a href="{{route('delete-blog',['id'=>$blog->id])}}">Delete</a>|<a href="{{route('edit-blog',['id'=>$blog->id])}}">Edit</a></td>
                                                </tr>                               
                                            @endforeach
                                            
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                <button class="btn btn-success">
                    <a href="{{route('creat-blog')}}">Creat Blog</a>
                </button>
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
      
@endsection