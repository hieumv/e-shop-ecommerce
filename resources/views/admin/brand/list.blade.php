@extends('admin.layouts.app')
@section('content')
    
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">{{$title}}</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"> <div class="col-12">
                                @if (session('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <h4>{{session('success')}}</h4>    
                                    </div>                            
                                    @endif

                                @if ($errors->any())
                                <div class="alert alert-warning alert-dismissible">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach    
                                </ul>    
                        </div>  
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List brand</h4>
                                
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Handle</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $brand)
                                            <tr>
                                                <th scope="row">{{$brand->id}}</th>
                                                <td>{{$brand->name}}</td>
                                                <td><a href="{{route('delete-category',['id'=>$brand->id])}}">Delete</a></td>
                                            
                                            </tr>
                                        @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                <button class="btn btn-success">
                    <a href="{{route('add-brand')}}">Add brand</a>
                </button>
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
      
@endsection