<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['id','name','price','id_category','id_brand','status','sale','company','hinhanh'	,'detail','created_at','id_user'];
    public $timestamps = true;
}
