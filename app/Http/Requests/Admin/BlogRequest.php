<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            
            'title' => 'required|min:12',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'description' => 'required|max:255',
            'content' => 'required|min:100',
    ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute Khong duoc de trong',
            'min' => ':attribute nho hon :min ky tu',
            'max' => ':attribute lon hon :max ky tu',

        ];
    }

    public function attributes()
    {
        return [
            'title' => "Tieu de",
            'image' => "Hinh anh",
            'description' => "Mo ta",
            'content' => "Noi dung"
        ];
    }
}
