<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            
                'name' => 'required|min:5',
                'avatar' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'email' => 'required|email',
        ];
    }

    public function messages(){
        return [
            'required' => 'ban phai nhap :attribute',
            'min' => ':attribute khong duoc be hon :min ky tu'
        ];
    }

    // public function attributes()
    // {
    //     return [
    //         'name' => 'Ten cau thu',
    //         'age' => 'Tuoi',
    //         'national' => 'Quoc gia',
    //         'position' => 'Vi tri',
    //         'salary' => 'Luong'

    //     ];
    // }

    
}
