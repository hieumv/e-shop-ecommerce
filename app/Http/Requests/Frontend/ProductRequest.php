<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'id_category' => 'required',
            'id_brand' => 'required',
            'status' => 'required',
            'image' => 'max:3',
            'image.*' => 'image|max:1024|mimes:jpg,jpeg,gif,png,webp',
            'image' => 'max:3',
            'sale' => 'nullable|integer|max:100|min:0',
            'detail' => 'nullable|string'
        ];
    }

    public function messages()
    {
        return [
            'image.*' => 'you only upload max 3 file'
        ];
    }
}
