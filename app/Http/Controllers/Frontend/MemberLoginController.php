<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\Frontend\LoginRequest;
use App\Models\User;

class MemberLoginController extends Controller
{
// show form login
    public function login(){
        $title = "Login";
        return view('frontend.member.login',compact('title'));
    }

    public function postLogin(LoginRequest $request){
        $data = $request->all();
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 1
        ];

        $remember = false;
        if($request->remember_me){
            $remember = true;
        }
        if(Auth::attempt($login,$remember)){
            return redirect()->back()->with('success','login thanh cong');
        }else{
            return redirect()->back()->with('error','Dang nhap that bai');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('member-login');
    }

}
