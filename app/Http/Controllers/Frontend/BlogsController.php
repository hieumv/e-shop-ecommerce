<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;


class BlogsController extends Controller
{
    public function index(){
        $title = "Blogs";
        $dataBlogs = Blog::Paginate(3);

        return view('frontend.blog.list',compact('title','dataBlogs'));
    }

    public function blogDetail($id){
        $title = "Blog Detail";
        $blog = Blog::find($id);
        // lay record truoc
        $previous = Blog::where('id', '<', $id)->max('id');
        // lay record tiep theo
        $next = Blog::where('id', '>', $id)->min('id');
        // xu ly rate
        $avgRate = Rate::where('id_blog','=', $id)->avg('rate');
        $avgRate = round($avgRate);
        
        // gui cmt
        $dataComment = Comment::where('id_blog',$id)->get();
        $dataRepComment = Comment::where('level','>',0)->get();

        return view('frontend.blog.blogDetail',compact('title','blog','previous','next','avgRate','dataComment','dataRepComment'));
    }

    public function rate_ajax(Request $request){
        // diem, id_user, id_blog
        $dataRate = $request->all();
        Rate::create($dataRate);
    }

    


}
