<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;


class CartController extends Controller
{
    public function cart(){
        $title = 'Cart';
        if(session()->has('cart')){
            $sessionCart = session()->get('cart');
            $qtyCart = 0;
            $totalOrder = 0;
            foreach($sessionCart as $key => $value){
                $qtyCart += $value['qty'];
                $totalOrder += intval($value['qty']*$value['price']);
            }
        }else{
            $sessionCart = [];
            $qtyCart = 0;
            $totalOrder = 0;
        }


        return view('Frontend.cart.cart',compact('title','sessionCart','qtyCart','totalOrder'));
    }

    public function decode($stringJs){
        $jsonstring = $stringJs;
        $array = json_decode($jsonstring,true);
        return $array;
    }

    public function cartDelete(Request $request){
        $id = $request->id;
        if(session()->has('cart')){
            $sessionCart = session()->get('cart',[]);
            foreach($sessionCart as $key => $value){
                if($value['id'] == $id){
                    unset($sessionCart[$key]);
                    session()->put('cart',$sessionCart);
                }
            }
        }
    }

    public function cartUp(Request $request){
        $id = $request->id;
        if(session()->has('cart')){
            $sessionCart = session()->get('cart',[]);
            foreach($sessionCart as $key => $value){
                if($value['id'] == $id){
                    $sessionCart[$key]['qty'] += 1;
                    session()->put('cart',$sessionCart);
                }
            }
        }
    }

    public function cartDown(Request $request){
        $id = $request->id;
        if(session()->has('cart')){
            $sessionCart = session()->get('cart',[]);
            foreach($sessionCart as $key => $value){
                if($value['id'] == $id){
                    $sessionCart[$key]['qty'] -= 1;
                    if($sessionCart[$key]['qty'] == 0){
                        unset($sessionCart[$key]);
                    }
                    session()->put('cart',$sessionCart);
                }
            }
        }
    }

    public function checkOut(){
        $title = 'Check out';
        return view('Frontend.cart.check-out',compact('title'));
    }

    
}
