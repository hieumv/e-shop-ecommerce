<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Comment;



class CommentController extends Controller
{
    public function postComment($id, Request $request){
        $id_blog = $id;
        $comment = new Comment;
        $comment->id_blog = $id_blog;
        $comment->id_user = Auth::id();
        $comment->cmt = $request->comment;
        $comment->level = 0;
        $comment->name = Auth::user()->name;
        $comment->avatar = Auth::user()->avatar;
        $comment->save();
        return redirect()->route('blogDetail',['id'=>$id_blog]);

    }
    public function repComment($id, Request $request){
        $id_blog = $id;
        $comment = new Comment;
        $comment->id_blog = $id_blog;
        $comment->id_user = Auth::id();
        $comment->cmt = $request->comment;
        // // level bang id cmt cha
        $comment->level = $request->level;
        $comment->name = Auth::user()->name;
        $comment->avatar = Auth::user()->avatar;
        $comment->save();
        return redirect()->route('blogDetail',['id'=>$id_blog]);
    }
}
