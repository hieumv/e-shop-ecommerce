<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\ProductRequest;
use Illuminate\Support\Facades\Auth;

use Image;

use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;





class ProductController extends Controller
{
    public function index(){
        $title = "List Products";
        $id_user = Auth::id();
        $data = Product::where('id_user',$id_user)->orderBy('id','desc')->get();
        foreach($data as $product){
            $jsonstring = $product['hinhanh'];
            $array = json_decode($jsonstring,true);
            $product['hinhanh'] = $array;
            $list_products[] = $product;
            
        }
        
        return view('frontend.product.list',compact('title','list_products'));
    }
    public function addProduct(){
        $title = "Add Product";
        $categories = Category::all();
        $brands = Brand::all();
        return view('frontend.product.add',compact('title','categories','brands'));
    }

    // public function postAdd(ProductRequest $request){
    //     $data = $request->all();
    //     dd($data);
    // }
    
    public function postAdd(ProductRequest $request){
        $data = $request->all();
        // xu ly hinh anh
        $uploadImage = [];
        if($request->hasfile('hinhanh'))
        {
            // xu ly hinh anh
            $uploadImage = $this->processImage($request->file('hinhanh'));
        }
        $data['hinhanh']=json_encode($uploadImage);
        $data['id_user'] = Auth::id();
        if(Product::create($data)){
            return redirect()->route('listProduct')->with('success', 'Add product success');
        }
    }

    public function editProduct($id){
        $title = "Edit Product";
        $product = Product::find($id);
        // decode json
        $jsonstring = $product['hinhanh'];
        $array = json_decode($jsonstring,true);
        $product['hinhanh'] = $array;

        $categories = Category::all();
        $brands = Brand::all();
        return view('frontend.product.edit',compact('title','categories','brands','product'));
    }

    public function postEdit(ProductRequest $request, $id){
        $data = $request->all();
         // lay data anh dang co trong database
         $product = Product::find($id);
         $jsonstring = $product['hinhanh'];
         $oldImage = json_decode($jsonstring,true);

        if(isset($data['image_delete'])){
            $deleteImage = $data['image_delete'];
            // kiem tra a muon xoa =>mang moi
            foreach($deleteImage as $key=>$value){
                if(in_array($value,$oldImage)){
                    unset($oldImage[$key]);
                }
            }
            $oldImage = array_values($oldImage);
        }

        $uploadImage = [];
        if($request->hasFile('hinhanh')){
            $count = count($data['hinhanh']) + count($oldImage);
            $numberFile = 3 - count($oldImage);
            if($count > 3){
                return back()->with('error-upload','Chi duoc upload '.$numberFile.' file');
            }
            // xu ly hinh anh
            // foreach($request->file('hinhanh') as $image)

                // {

                //     $name = $image->getClientOriginalName();
                //     $name_2 = "hinh_85_".$image->getClientOriginalName();
                //     $name_3 = "hinh_329_".$image->getClientOriginalName();

                    
                //     // creat 3 path
                //     $path = public_path('/upload/product/' . $name);
                //     $path2 = public_path('/upload/product/' . $name_2);
                //     $path3 = public_path('/upload/product/' . $name_3);

                    

                //     // save file to 3 path
                //     Image::make($image->getRealPath())->save($path);
                //     Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                //     Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                //     $uploadImage[] = $name;
                // }
            $this->processImage($request->file('hinhanh'));

        }
        $oldImage = array_merge($oldImage,$uploadImage);
        if(count($oldImage) == 0){
            return back()->with('error-upload','Phai upload it nhat 1 file');
        }
        $data['hinhanh']=json_encode($oldImage);
        if($product->update($data)){
            return redirect()->route('listProduct')->with('success', 'Edit product success');
        }
        
    }

    public function processImage($imageArr){

        foreach($imageArr as $image)
                {

                    $name = $image->getClientOriginalName();
                    $name_2 = "hinh_85_".$image->getClientOriginalName();
                    $name_3 = "hinh_329_".$image->getClientOriginalName();

                    
                    // creat 3 path
                    $path = public_path('/upload/product/' . $name);
                    $path2 = public_path('/upload/product/' . $name_2);
                    $path3 = public_path('/upload/product/' . $name_3);

                    

                    // save file to 3 path
                    Image::make($image->getRealPath())->save($path);
                    Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                    Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                    $uploadImage[] = $name;
                   
                }
        return $uploadImage;
    }

    public function deleteProduct($id){
        $product = Product::find($id);
        if($product){
            $product->delete();
            return redirect()->route('listProduct')->with('success', 'Delete product success');
        }
    }
    
}
