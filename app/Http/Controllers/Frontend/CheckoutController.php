<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Models\User;
use App\Models\History;
use Mail;


class CheckoutController extends Controller
{
    public function index(){
        if(Auth::check()==true){
            $title = 'Checkout';
            if(session()->has('cart')){
                $sessionCart = session()->get('cart');
                $qtyCart = 0;
                $totalOrder = 0;
                foreach($sessionCart as $key => $value){
                    $qtyCart += $value['qty'];
                    $totalOrder += intval($value['qty']*$value['price']);
                }
            }else{
                $sessionCart = [];
                $qtyCart = 0;
                $totalOrder = 0;
            }
            return view('frontend.checkout.check-out',compact('title','sessionCart','qtyCart','totalOrder'));
        }else{
            
            return redirect()->route('loginCheckOut');
        }
    }

    public function loginCheckout(){
        $title = 'Login Checkout';
        return view('frontend.checkout.login-checkout',compact('title'));
    }

    public function postLoginCheckout(RegisterRequest $request){
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        $login = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if($user->save()){
            if(Auth::attempt($login)){
                return redirect()->route('checkOut');
            }
            
        }else{
            return redirect()->back()->withErrors('Them nguoi dung that bai');}
        
    }

    public function checkOut(Request $request){
        // save data history to History table
        $dataHistory = [];
        $dataHistory['name'] = $request->name;
        $dataHistory['email'] = $request->email;
        $dataHistory['phone'] = $request->phone;
        $dataHistory['price'] = 1;
        $dataHistory['id_user'] = Auth::id();
        History::create($dataHistory);
        // get cart from session to send info cart to mail
        $totalOrder = 0;
        if(session()->has('cart')){
            $sessionCart = session()->get('cart',[]);
            foreach($sessionCart as $key => $value){
                $totalOrder += intval($value['qty']*$value['price']);
            }
        }

        $name = 'Hieu mai';
        Mail::send('frontend.checkout.mail',compact('name','dataHistory','totalOrder','sessionCart'),function($email){
            $email->to('hieumvpd04214@fpt.edu.vn','Hiwu mai');
        });
        return redirect()->back()->with('success','Dang xu ly don hang');

    }



}
