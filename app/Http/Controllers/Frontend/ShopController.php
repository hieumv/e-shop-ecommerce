<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Collection;

use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;

class ShopController extends Controller
{
    public function index(){
        $title = "Index";
        $dataProduct = Product::orderBy('created_at','desc')->take(6)->get();
        foreach($dataProduct as $product){
            $jsonstring = $product['hinhanh'];
            $array = json_decode($jsonstring,true);
            $product['hinhanh'] = $array;
            $products[] = $product;
            
        }
        return view('frontend.index',compact('title','products'));
    }

    public function detail($id){
        $title = "Product Detail";
        $product = Product::find($id);

        $product['hinhanh'] = $this->decode($product['hinhanh']);
        return view('frontend/product/detail',compact('title','product'));
    }

    public function decode($stringJs){
        $jsonstring = $stringJs;
        $array = json_decode($jsonstring,true);
        return $array;
    }

    public function addToCart(Request $request){
        $id = $request->id;
        $product = Product::find($id);
        $item['name'] = $product->name;
        $item['price'] = $product->price;
        $item['hinhanh'] = $this->decode($product->hinhanh);
        $item['id'] = $id;
        

        if($request->qty){
            $item['qty'] = $request->qty;
        }else{
            $item['qty'] = 1;
        }

        $flag = 1;
        if(session()->has('cart')){
            $cart = session()->get('cart',[]);
            foreach($cart as $key => $value){
                if($value['id'] == $id){
                    $cart[$key]['qty'] += $item['qty'];
                    session()->put('cart',$cart);
                    $flag = 0;
                    break;
                }
            }
        }

        if($flag == 1){
            session()->push('cart',$item);
        }


    }

    public function searchProduct(Request $request){
        $title = 'Search';
        $keyword = $request->keyword;
        $dataProduct = Product::where('name', 'like', '%'.$keyword.'%')->get();
        foreach($dataProduct as $product){
            $jsonstring = $product['hinhanh'];
            $array = json_decode($jsonstring,true);
            $product['hinhanh'] = $array;
            $products[] = $product;
            
        }
        return view('frontend.search.search',compact('title','products'));
    }

    public function searchAdvanced(){
        $title = 'Search';
        $brands = Brand::all();
        $categories = Category::all();
        return view('frontend.search.advanced',compact('title','brands','categories'));
    }

    public function postSearch(Request $request){
        $title = 'Search';
        $brands = Brand::all();
        $categories = Category::all();
        
        $data = Product::query();
        $req = 0;
        if($request->category_id) {
            $data->where('id_category',$request->category_id);
            $req = 1;
        }
       
        if($request->brand_id){
            $data->where('id_brand', $request->brand_id);
            $req = 1;
        }

        if($request->name){
            $data->where('name', 'like','%'.$request->name.'%');
            $req = 1;
        }
        
        if($request->price){
            if($request->price == 2){
                $data->whereBetween('price', [10000, 20000]);
            }

            if($request->price == 3){
                $data->where('price', '>', [20000]);
            }
            $req = 1;
        }

        $dataProduct = $data->get();

        if($req == 0){
            $dataProduct = Product::Paginate(6);
        }
        
        foreach($dataProduct as $product){
            $jsonstring = $product['hinhanh'];
            $array = json_decode($jsonstring,true);
            $product['hinhanh'] = $array;
            $products[] = $product;
            // $products = new Collection($product);
        }

        return view('frontend.search.advanced',compact('title','products','brands','categories'));

        
    }

    public function searchPrice(Request $request){
        $min = $request->min;
        $max = $request->max;

        $data = Product::query();
        $data->whereBetween('price',[$min,$max]);
        $dataProduct = $data->get();
        foreach($dataProduct as $product){
            $jsonstring = $product['hinhanh'];
            $array = json_decode($jsonstring,true);
            $product['hinhanh'] = $array;
            $products[] = $product;
        }

        $response = [
            'status' => 'success',
            'products' => $products
        ];

        return response()->json($response);
        }


    
}
