<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\UpdateProfileRequest;


use App\Models\User;
use App\Models\Country;


class AccountController extends Controller
{
    public function index(){
        $title = "Account";
        $idUser = Auth::id();
        $user = User::find($idUser);
        $countries = Country::all();
        return view('Frontend.account.account',compact('title','user','countries'));
    }

    public function postUpdateUser(UpdateProfileRequest $request){
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        

        // lay du lieu tu form
        $data = $request->all();
        
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
       
        // xu ly mat khau
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
        // cap nhat thong tin nguoi dung
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','update success');
        }else{
            return redirect()->back()->withErrors('update fail');
        }
    }

    public function myProduct(){

    }

    
}
