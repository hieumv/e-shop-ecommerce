<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Frontend\RegisterRequest;
use App\Models\User;

class MemberRegisterController extends Controller
{
    public function register(){
        $title = "Register";
        return view('frontend.member.register',compact('title'));
    }

    public function postRegister(RegisterRequest $request){
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        if($user->save()){
            return redirect()->back()->with('success','Dang ky nguoi dung thanh cong');
        }else{
            return redirect()->back()->withErrors('Them nguoi dung that bai');        }
        
    }
}
