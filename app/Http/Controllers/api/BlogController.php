<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;


class BlogController extends Controller
{
    public function list(){
        $listBlog = Blog::all();
        return response()->json([
            'blog' => $listBlog
        ]);
    }

    public function show($id)
    {

        if(!empty($id)) {

            $blogDetail = Blog::find($id);

            return response()->json([
                'status' => 200,
                'data' => $blogDetail
            ]);
        }
    }
}
