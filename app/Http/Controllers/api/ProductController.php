<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;


class ProductController extends Controller
{
    public function list(){
        $dataProduct = Product::all();
        return response()->json([
            'products' => $dataProduct
        ]);
    }
    
    public function productDetail($id){
        $productDetail = Product::find($id);
        return response()->json([
            'status' => 200,
            'data' => $productDetail
        ]);
    }
}
