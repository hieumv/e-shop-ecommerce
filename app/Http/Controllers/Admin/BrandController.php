<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\CountryRequest;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $title = "Brand";
        $data = Brand::all();
        // dd($data);
        return view('admin.brand.list',compact('title','data'));
    }

    public function add(){
        $title = "Add Brand";
       return view('admin.brand.add',compact('title'));
    }
    public function postAdd(CountryRequest $request){
       $data = $request->all();
       if(Brand::create($data)){
        return redirect()->route('list-brands')->with('success','Add success');
    }else{
        return redirect()->back()->withErrors('Add fail');
    }
    }

    public function delete($id){
        $Brand = Brand::find($id);
        if($Brand){
            $Brand->delete();
            return redirect()->route('list-categories')->with('success','Delete success');
        }else {
            return redirect()->back()->withErrors('Delete fail');
        }
    }
}
