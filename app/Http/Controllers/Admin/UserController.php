<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;


use App\Http\Requests\Admin\UpdateProfileRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    // hien thi form update user
    public function index(){
        $title = "Profile";
        $userId = Auth::id();
        $user = User::find($userId);
        $userDetail = $user->only($user->getFillable());
        // dd($userDetail);
        $countries = Country::all();
        // gui mot mang qua detail user qua view
        return view('admin.user.profile',compact('title','userDetail','countries'));
    }

    public function UpdateUser(UpdateProfileRequest $request){
        // lay thong tin nguoi dung theo id dang nhap
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        
        // lay du lieu tu form
        $data = $request->all();
        
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
       
        // xu ly mat khau
        if($data['password']){
            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }
        // cap nhat thong tin nguoi dung
        if($user->update($data)){
            if(!empty($file)){
                $file->move('upload/user/avatar',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','update success');
        }else{
            return redirect()->back()->withErrors('update fail');
        }
        
    }

    public function checkAuth(){
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        dd($user);
    }

    

}
