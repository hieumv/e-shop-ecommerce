<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\CountryRequest;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Country;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.member');
    }

    public function index(){
        $title = "Country";
        $data = Country::all();
        // dd($data);
        return view('admin.country.list',compact('title','data'));
    }

    public function add(){
        $title = "Add Country";
       return view('admin.country.add',compact('title'));
    }
    public function postAdd(CountryRequest $request){
       $data = $request->all();
       if(Country::create($data)){
        return redirect()->route('list-countries')->with('success','Add success');
    }else{
        return redirect()->back()->withErrors('Add fail');
    }
    }

    public function delete($id){
        $country = Country::find($id);
        if($country){
            $country->delete();
            return redirect()->route('list-countries')->with('success','Delete success');
        }else {
            return redirect()->back()->withErrors('Delete fail');
        }
    }
}
