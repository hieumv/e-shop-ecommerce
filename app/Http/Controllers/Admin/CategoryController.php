<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\CountryRequest;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $title = "Category";
        $data = Category::all();
        // dd($data);
        return view('admin.category.list',compact('title','data'));
    }

    public function add(){
        $title = "Add Category";
       return view('admin.category.add',compact('title'));
    }
    public function postAdd(CountryRequest $request){
       $data = $request->all();
       if(Category::create($data)){
        return redirect()->route('list-categories')->with('success','Add success');
    }else{
        return redirect()->back()->withErrors('Add fail');
    }
    }

    public function delete($id){
        $category = Category::find($id);
        if($category){
            $category->delete();
            return redirect()->route('list-categories')->with('success','Delete success');
        }else {
            return redirect()->back()->withErrors('Delete fail');
        }
    }
}
