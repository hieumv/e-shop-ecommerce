<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Requests\Admin\BlogRequest;


class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    

    public function index(){
        $title = "Blog";
        $dataBlogs = Blog::all();
        return view('admin.blog.list',compact('title','dataBlogs'));
    }

    public function creat(){
        $title = "Creat Blog";
        return view('admin.blog.creat',compact('title'));
    }

    public function postCreat(BlogRequest $request){

        // xu ly du lieu tu form
        $data = $request->all();
        
        $file = $request->image;
        if(!empty($file)){
            $data['image'] = $file->getClientOriginalName();
        }
        // them blog
        if(Blog::create($data)){
            if(!empty($file)){
                $file->move('upload/Blog/Image',$file->getClientOriginalName());
            }
            return redirect()->route('blog')->with('success','Creat blog success');
        }else{
            return redirect()->back()->withErrors('update fail');
        }
        
        
    }

    // delete
    public function delete($id){
        $content = Blog::find($id);
        if($content){
            $content->delete();
            return redirect()->route('blog')->with('success','Delete success');
        }
    }
    // show form edit
    public function edit($id){
        $title = "Edit Blog";
        $content = Blog::find($id);
        return view('admin.blog.edit',compact('title','content'));
       
    }

    // 
    public function postEdit(BlogRequest $request, $id){
        $content = Blog::find($id);
        $dataRequest = $request->all();

        $file = $request->image;
        if(!empty($file)){
            $content['image'] = $file->getClientOriginalName();
        }
        
        if($content->update($dataRequest)){
            if(!empty($file)){
                $file->move('upload/Blog/Image',$file->getClientOriginalName());
            }
            return redirect()->route('blog')->with('success','Edit success');
        }else{
            return redirect()->back()->withErrors('Edit fail');
        }
    }

    public function showBlog($id){
        $title = "blogDetail";
        $content = Blog::find($id);
        return view('admin.blog.detail',compact('title','content'));
    }
    
   
    
}
